const { openBrowser, goto, textBox, into, write, $, click, press, below, text, closeBrowser } = require('taiko');
const assert = require('assert').strict;

(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/kiosk/#/kiosk");
        await write("aishwarya@zendynamix.com", into(textBox({placeholder:"Username"})));
        await write("testuser@123", into(textBox({placeholder:"password"})));
        await click($("//button[contains(text(),'LOGIN')]"));
        await click($("//h1[contains(text(),'To Order Food')]"));
        await click($("//div[contains(@class,'select-branch')]"));
        await press(['ArrowDown', 'ArrowDown']);//Blr Airport
        await press('Enter')
        await click($("//p[contains(@class,'increase')]"), below("Peach"));
        await click($("//p[contains(@class,'increase')]"), below("Strawberry Cupcake"));
	    assert.ok(await text('246').exists(0,0));
        await click("Checkout");
	    assert.ok(await text('290.28').exists(0,0));
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
