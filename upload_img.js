const { openBrowser, goto, textBox, into, write, click, $, fileField, to, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/v2/#/KIOSK");
	;
        await write("aishwarya@zendynamix.com", into(textBox({id:"inputEmail"})));
        await write("testuser@123", into(textBox({id:"inputPassword"})));
        await click("Login");
        // await click("KIOSK");
        await click($("(//button[contains(@type,'button')])[50]"));
        await click("Product");
        await write("French Croissant", into(textBox({id:"searchBox"})));
        await click("Almond French Croissant");
        // await click($("(//button[contains(@class,'p-element p-button p-component p-button-icon-only')])[3]"));
        await click($("//button[contains(@id,'thumbnail-img-upload-btn')]"));
        await click("Display");
        await click("Display Image");
        await click("Choose");
        await click("Upload");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
