const { openBrowser, textBox, into, write, click, $, closeBrowser } = require('taiko');
(async () => {
    try {

        //Removing a product through Remove option and adding back the same product 
    await openBrowser();
    await goto("https://demo.iotzen.app/v2");
    await write("aishwarya@zendynamix.com", into(textBox({id:"inputEmail"})));
	await write("testuser@123", into(textBox({id:"inputPassword"})));
	await click("Login");
    await click($("(//button[contains(@type,'button')])[50]"));
    await click("Store");
    let name = await $("//td[contains(@class,'p-element p-editable-column ng-star-inserted')]");
    let prod_name = await name.text();
    console.log(prod_name);

    await click($("(//div[contains(@role,'checkbox')])[2]"));
    await click("Remove");
    await click("Yes");
    await click("Products");
    await click($("(//div[contains(@role,'checkbox')])[23]"));
    await click("To Store");
    await click($("(//button[contains(@type,'button')])[37]"));
    await click($("//input[contains(@id,'searchBox')]"));
    await write(prod_name);
    await click($("(//div[contains(@role,'checkbox')])[2]"));
    await click($("(//span[contains(text(),'Groups')])[2]"));
    await click($("(//div[contains(text(),'Select Groups to Add')])"));
    await click("Top Selling");
    await click("Add");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
