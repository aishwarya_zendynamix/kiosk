const { openBrowser, click, $, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/v2/#/KIOSK");
        await write("aishwarya@zendynamix.com", into(textBox({id:"inputEmail"})));
        await write("testuser@123", into(textBox({id:"inputPassword"})));
        await click("Login");
        await click("Store");
        await click($("(//div[contains(@role,'checkbox')])[3]"));
        await click("Products");
        await click($("(//div[contains(@role,'checkbox')])[23]"));
        await click("To Store");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
