const { openBrowser, $, click, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/kiosk/#/");
        await write("aishwarya@zendynamix.com", into(textBox({placeholder:"Username"})));
        await write("testuser@123", into(textBox({placeholder:"password"})));
        await click($("//button[contains(text(),'LOGIN')]"));
        await click($("//h1[contains(text(),'To Order Food')]"));
        await click($("//div[contains(@class,'select-branch')]"));
        await press(['ArrowDown', 'ArrowDown']);//Blr Airport
        await press('Enter');
        await click("Beverages");
        await click("Mango");
        await click($("//p[contains(@class,'increase')]"));
        await click($("//p[contains(@class,'increase')]"));
        await click($("//p[contains(@class,'increase')]"));
        await click($("(//p[contains(@class,'increase')])[3]"));
        await click("Checkout");
        await click("Pay in Store");
        await click("Confirm");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
