const { openBrowser, goto, textBox, into, write, click, $, below, closeBrowser, openTab } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/kiosk/#/");
        await write("iotzensok", into(textBox({placeholder:"Username"})));
        await write("sok@admin", into(textBox({placeholder:"password"})));
        await click($("//button[contains(text(),'LOGIN')]"));
        await click($("//h1[contains(text(),'To Order Food')]"));
        await click($("//div[contains(@class,'select-branch')]"));
        // await press(['ArrowDown', 'ArrowDown']);//Blr Airport
        await press(['ArrowDown']);//Blr Airport
        await press('Enter')
        await click("Peach");
        await click($("//p[contains(@class,'increase')]"));
        await click($("//p[contains(@class,'increase')]"));
        await click($("//p[contains(@class,'increase')]"));
        await click($("(//p[contains(@class,'increase')])[3]"));
        await click("Checkout");
        // // await click("Pay in Store");
        let orderid = await $("//label[contains(@id,'orderId')]").element(0);
        let con = await orderid.text();
        console.log(con);
        let arr = con.slice(7);
        console.log(arr);

        await openTab("https://demo.iotzen.app/v2/#/KIOSK/");
        await write("iotzensok", into(textBox({id:"inputEmail"})));
        await write("sok@admin", into(textBox({id:"inputPassword"})));
        await click("Login");
        await click($("(//button[contains(@type,'button')])[50]"));
        await click($("//div[contains(@role,'button')]"));
        // await click("Blr Airport-Cart");
        await click("Indiranagar");

        
        await click($("//input[contains(@id,'searchBox')]"));
        // await write("1187");
        await write(arr);
        await click("NOT PAID");
        await click("Cash Payment");
        await click("Update Payment Status");
        await click("PLACED");
        await click("Prepared");
        await click("Prepared");
        await click("Serviced");

        // let element1 = await $("//div[contains(@id,'row-clickable')]").element(0);
        // const con1 = console.log(await element.text());
        // assert.ok(con,con1);
        // await click(textBox({id:"searchBox"}));
        // await write();
        // await click("Confirm");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
