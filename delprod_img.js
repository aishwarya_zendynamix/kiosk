const { openBrowser, click, $, below, near, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/v2/#/KIOSK");
        await write("aishwarya@zendynamix.com", into(textBox({id:"inputEmail"})));
        await write("testuser@123", into(textBox({id:"inputPassword"})));
        await click("Login");
        // await click("KIOSK");
        await click($("(//button[contains(@type,'button')])[50]"));
        await click("Product");
        await write("French Croissant", into(textBox({id:"searchBox"})));
        await click("Almond French Croissant");
        await click($("//button[contains(@id,'custom-galleria-delete')]"))
        await click("Yes");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
