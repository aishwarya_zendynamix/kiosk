const { openBrowser, click, $, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await click("Store");
        await click($("(//div[contains(@role,'checkbox')])[3]"));
        await click("Remove");
        await click("No");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
