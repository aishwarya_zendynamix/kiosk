const { openBrowser, goto, textBox, into, write, click, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/v2/#/KIOSK");
        await write("aishwarya@zendynamix.com", into(textBox({id:"inputEmail"})));
        await write("testuser@123", into(textBox({id:"inputPassword"})));
        await click("Login");
        // await click("KIOSK");
        await click("Order");
        await write("1078", into(textBox({id:"searchBox"})));
        await click("NOT PAID");
        await click("Cash Payment");
        await click("Yes");
        await click("PLACED");
        await click("Prepared");
        await click("Prepared");
        await click("Serviced");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
