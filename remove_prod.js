const { openBrowser, click, $, textBox, into, write, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/v2/#/KIOSK");
        await write("aishwarya@zendynamix.com", into(textBox({id:"inputEmail"})));
        await write("testuser@123", into(textBox({id:"inputPassword"})));
        await click("Login");
        await click("KIOSK");
        await click("Product");
        await click("Lemon");
        await click($("//span[contains(@class,'p-inputswitch-slider')]"));
        await click("Remove");
        await click("Yes");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
