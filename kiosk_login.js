const { openBrowser, goto, textBox, into, write, click, closeBrowser } = require('taiko');
(async () => {
    try {
	openBrowser;
        await openBrowser();
        await goto("https://demo.iotzen.app/v2/#/KIOSK");
        await write("aishwarya@zendynamix.com", into(textBox({id:"inputEmail"})));
        await write("testuser@123", into(textBox({id:"inputPassword"})));
        await click("Login");
        await click("KIOSK");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
