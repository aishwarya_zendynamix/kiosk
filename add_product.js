const { openBrowser, goto, textBox, into, write, click, above, closeBrowser } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("https://demo.iotzen.app/v2/#/KIOSK");
        await write("aishwarya@zendynamix.com", into(textBox({id:"inputEmail"})));
        await write("testuser@123", into(textBox({id:"inputPassword"})));
        await click("Login");
        await click("KIOSK");
        await click("Product");
        await click("Products", above("Price"));
        await write("Aishwarya", into(textBox({id:"basicInput"})));
        await write("1234", into(textBox({placeholder:"Enter Sku Code"})));
        await click("Submit");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
